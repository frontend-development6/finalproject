import React, { useState } from 'react'
import { Grid, CardMedia, styled } from '@mui/material';
function AboutUs() {

  const CardMediaStyle = {
    maxWidth: 200, 
    maxHeight: 200, 
    margin: '2%', 
    border: 'solid 7px gray',
    borderRadius: '50%',
    boxShadow: '5px 10px 20px 5px Green inset',
  }
  const Widget = styled('div')(`
      display: flex;
      align-items: center;
      flex-direction: column;
      animation-duration: 0.5s;
      animation-name: animate-fade;
      animation-delay: 0.5s;
      animation-fill-mode: backwards;
        
      @keyframes animate-fade {
          0% {
            opacity: 0;
          }
          100% {
            opacity: 1;
          }
      }
    `,
  );
  return (
    <Grid container sx={{display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: '100px'}}>
      <Grid item md={3} sx={{display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column'}}>
        <Widget>
          <CardMedia
              component="img"
              style={CardMediaStyle}
              image="https://ca.slack-edge.com/T032KES7072-U03NUU1LRL7-a203e4b6ef7a-512"
              alt="Album image"
            />
          <h1>Heidy</h1>
        </Widget>
      </Grid>
      <Grid item md={3} sx={{display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column'}}>
          <Widget>
            <CardMedia
              component="img"
              style={CardMediaStyle}
              image="https://ca.slack-edge.com/T032KES7072-U03PCSV4FU3-e853aef35815-512"
              alt="Album image"
            />
            <h1>Juan Jose</h1>
          </Widget>
      </Grid>
      <Grid item md={3} sx={{display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column'}}>
        <Widget>
          <CardMedia
              component="img"
              style={CardMediaStyle}
              image="https://ca.slack-edge.com/T032KES7072-U03P6J160JZ-32ec7d15ee26-512"
              alt="Album image"
            />
          <h1>Rafael</h1>
        </Widget>
      </Grid>
      <Grid item md={3} sx={{display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column'}}>
          <Widget>
            <CardMedia
              component="img"
              style={CardMediaStyle}
              image="https://ca.slack-edge.com/T032KES7072-U03NUU1JY07-cb0d05a7ba2e-512"
              alt="Album image"
            />
            <h1>Raquel</h1>
          </Widget>
      </Grid>
    </Grid>
  )
}

export default AboutUs