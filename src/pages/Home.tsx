import Cards from '../Components/Cards/Cards';
import React, { useState } from 'react';
import MusicPlayerVertical from '../Layouts/MusicPlayer/VerticalMusicPlayer/MusicPlayerVertical';
import { Grid } from '@mui/material';
import ListAlbums from '../Components/Cards/ListAlbums';
import CardArtistsLayer from '../Components/Cards/CardArtistsLayer';
import MusicPlayerHorizontal from '../Layouts/MusicPlayer/HorizontalMusicPlayer/MusicPlayerHorizontal';
import Container from '@mui/material/Container';

const artistsAlbums = 'ARTISTALBUMS';


import Switch from '@mui/material/Switch';
import Slide from '@mui/material/Slide';

const musicPlayerVerticalView = (
  <div>
    <MusicPlayerVertical></MusicPlayerVertical>
  </div>
);

function Home() {
  const [artistId, setArtistId] = useState<number>(0);
  const page = [artistsAlbums];

  const artistHandle = (artistKey: number) => {
    console.log("artistKey: ", artistKey);
    setArtistId(artistKey);
  }

  const [checked, setChecked] = React.useState(false);

  const handleChange = () => {
    setChecked((prev) => !prev);
  };

  let [currentTrack, setCurrentTrack] = useState(0);
  const [pausedTrack, setPausedTrack] = useState(true);
  const [audioTrack, setAudioTrack] = useState<any | null>(null);
  /* const [pausedTrackOut, setPausedTrackOut] = useState(true); */
  return (
    <Grid container>
      <Grid item xs={3} sx={{ borderRight: 1, paddingX: '10px' }}>
        <CardArtistsLayer cards={page} click={artistHandle}></CardArtistsLayer>
      </Grid>
      <Grid item xs={9}>
        <Grid container direction="row">
          <Grid item xs={12}>
            <Container maxWidth="md" sx={{ margin: '2%' }}>
              <ListAlbums
                artistId={artistId}
                currentTrack={currentTrack}
                pausedTrack={pausedTrack}
                audioTrack={audioTrack}
              /* pausedTrack={setPausedTrack} */
              ></ListAlbums>
            </Container>
          </Grid>
          <Grid item xs={12} sm={12} md={4}>
            <Slide direction="down" in={checked} mountOnEnter unmountOnExit>
              {musicPlayerVerticalView}
            </Slide>

          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <MusicPlayerHorizontal
          control={<Switch checked={checked} onChange={handleChange} />}
          currentTrack={setCurrentTrack}
          pausedTrack={setPausedTrack}
          audioTrack={setAudioTrack}
        />
      </Grid>
    </Grid>
  );
}

export default Home;
