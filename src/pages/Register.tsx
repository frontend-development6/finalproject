import { Container } from '@mui/material';
import React, { useState } from 'react';
import AlbumRegisterForm from '../Components/Register/AlbumRegisterForm';
import ArtistRegisterForm from '../Components/Register/ArtistRegisterForm';
import NavBar from '../Components/NavBar';
import SongRegisterForm from '../Components/Register/SongRegisterForm';

const formRegArtist = 'ARTIST';
const formRegSong = 'SONG';
const formRegAlbum = 'ALBUM';

function Register() {
  const [selectedPage, setSelectedPage] = useState(formRegArtist);
  const page = [formRegArtist, formRegAlbum, formRegSong];
  const navHandle = (navKey: string) => {
    setSelectedPage(navKey);
  };
  return (
    <div>
      {/* <NavBar buttons={page} click={navHandle}></NavBar> */}
      {selectedPage == formRegArtist ? (
        <Container sx={{ mt: 3 }}>
          <ArtistRegisterForm></ArtistRegisterForm>
        </Container>
      ) : (
        <></>
      )}

      {selectedPage == formRegSong ? (
        <Container sx={{ mt: 3 }}>
          <SongRegisterForm></SongRegisterForm>
        </Container>
      ) : (
        <></>
      )}

      {selectedPage == formRegAlbum ? (
        <Container sx={{ mt: 3 }}>
          <AlbumRegisterForm></AlbumRegisterForm>
        </Container>
      ) : (
        <></>
      )}
    </div>
  );
}

export default Register;
