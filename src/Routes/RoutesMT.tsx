import React from 'react'
import { Routes, Route } from 'react-router-dom';
import AboutUs from '../pages/AboutUs';
import Home from '../pages/Home';
import Playlists from '../pages/Playlists';

function RoutesMT() {
  return (
	<Routes>
		<Route path='/' element={ <Home/> }></Route>
		<Route path='/playlist' element={ <Playlists/> }></Route>
		<Route path='/about-us' element={ <AboutUs/> }></Route>
		{/* <Route path='/register' element={ <Register/> }></Route>
		<Route path='/about-us' element={ <AboutUs/> }></Route> */}
	</Routes>
  )
}

export default RoutesMT