import React from 'react';
import { screen, render, fireEvent, getByRole, waitFor } from '@testing-library/react';
import CardPlaylists from '../Components/Cards/CardPlaylist';
import '@testing-library/jest-dom/extend-expect';

describe('CardPlaylists render Page', () => {
  it('Must display a title', () => {
    render(<CardPlaylists />);
    screen.debug();
    const title = screen.queryByText(/by My Tunez/i);
    expect(title).toBeInTheDocument();
  });
});