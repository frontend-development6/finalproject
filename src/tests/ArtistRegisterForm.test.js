import React from 'react';
import { screen, render, fireEvent, getByRole, waitFor } from '@testing-library/react';
import ArtistRegisterForm from '../Components/Register/ArtistRegisterForm';
import UserEvent from '@testing-library/user-event';
import { act } from 'react-dom/test-utils';
import '@testing-library/jest-dom/extend-expect';

describe('ArtistRegisterFomr', () => {
  test('Must display a title', () => {
    render(<ArtistRegisterForm />);
    screen.debug();
    const title = screen.queryByText(/artist form/i);
    expect(title).toBeInTheDocument();
  });

  /*
    test('Must display a TextField  for title', ()=>{
        render(<ArtistRegisterForm/>);
        const textField = screen.getByRole('Button', {name:/Register/i});
        expect(textField).toBeInTheDocument();
    })
    */
});

describe('Artist Register Form render Page', () => {
  it('Must display a title', () => {
    render(<ArtistRegisterForm />);
    screen.debug();
    const title = screen.queryByText(/artist form/i);
    expect(title).toBeInTheDocument();
  });


  it('render 3 TextField components', () => {
    const { getByLabelText } = render(<ArtistRegisterForm />);
    expect(getByLabelText(/Name/i)).toBeInTheDocument();
    expect(getByLabelText(/members/i)).toBeInTheDocument();
    expect(getByLabelText(/Web Site/i)).toBeInTheDocument();
  });

  it('renders a Register button type-submit', () => {
    const { getByText } = render(<ArtistRegisterForm />);
    expect(getByText('Register')).toBeInTheDocument();
  });

  it('validate Artist Form TextField and Select, and provides error messages', async () => {
    const { getByTestId, getByText } = render(<ArtistRegisterForm />);

    await act(async () => {
      fireEvent.change(screen.getByLabelText(/name/i), {
        target: { value: '' }
      });   

      fireEvent.change(screen.getByLabelText(/members/i), {
        target: { value: '' }
      });

      fireEvent.change(screen.getByLabelText(/Web Site/i), {
        target: { value: '' }
      });
    });

    await act(async () => {
      fireEvent.submit(getByTestId('form'));
    });

    expect(getByText('Name is required')).toBeInTheDocument();
    expect(getByText('Members are Required')).toBeInTheDocument();
    expect(getByText('Web Site is Required')).toBeInTheDocument();
  });

  it('should submit when form inputs contain text', async () => {
    const { getByTestId, queryByText } = render(<ArtistRegisterForm />);

    await act(async () => {
      fireEvent.change(screen.getByLabelText(/name/i), {
        target: { value: 'Martin Garrix' }
      });

      fireEvent.change(screen.getByLabelText(/members/i), {
        target: { value: 'Martijn Gerard Garritsen' }
      });
      fireEvent.change(screen.getByLabelText(/Web Site/i), {
        target: { value: 'https://martingarrix.com/' }
      });
    });

    await act(async () => {
      fireEvent.submit(getByTestId('form'));
    });

    expect(getByText('Name is required')).toBeInTheDocument();
    expect(getByText('Members are Required')).toBeInTheDocument();
    expect(getByText('Web Site is Required')).toBeInTheDocument();
  });
});
