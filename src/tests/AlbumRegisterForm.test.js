import React from 'react';
import { screen, render, fireEvent, getByRole, waitFor } from '@testing-library/react';
import AlbumRegisterForm from '../Components/Register/AlbumRegisterForm';
import { act } from 'react-dom/test-utils';
import '@testing-library/jest-dom/extend-expect';

describe('Album Register Form render Page', () => {
  it('render 3 TextField components', () => {
    const { getByLabelText } = render(<AlbumRegisterForm />);
    expect(getByLabelText(/title/i)).toBeInTheDocument();
    expect(getByLabelText(/year/i)).toBeInTheDocument();
    expect(getByLabelText(/Genre/i)).toBeInTheDocument();
  });

  it('renders a Upload File', () => {
    const { getByText } = render(<AlbumRegisterForm />);
    expect(getByText('Upload File')).toBeInTheDocument();
  });

  it('renders a Register button type-submit', () => {
    const { getByText } = render(<AlbumRegisterForm />);
    expect(getByText('Register')).toBeInTheDocument();
  });

  it('validate Album Form TextField, and provides error messages', async () => {
    const { getByTestId, getByText } = render(<AlbumRegisterForm />);

    await act(async () => {
      fireEvent.change(screen.getByLabelText(/title/i), {
        target: { value: '' }
      });

      fireEvent.change(screen.getByLabelText(/year/i), {
        target: { value: '' }
      });
    });

    await act(async () => {
      fireEvent.submit(getByTestId('form'));
    });

    expect(getByText('Title is required')).toBeInTheDocument();
    expect(getByText('Year is Required')).toBeInTheDocument();
  });
});
