import React from 'react';
import { screen, render, fireEvent, getByRole, waitFor } from '@testing-library/react';
import SongRegisterForm from '../Components/Register/SongRegisterForm';
import { act } from 'react-dom/test-utils';
import '@testing-library/jest-dom/extend-expect';

describe('Song Register Form render Page', () => {
  it('render 3 TextField components', () => {
    const { getByLabelText } = render(<SongRegisterForm />);
    expect(getByLabelText(/title/i)).toBeInTheDocument();
    expect(getByLabelText(/Year Release/i)).toBeInTheDocument();
    expect(getByLabelText(/Duration/i)).toBeInTheDocument();
  });

  it('renders a Upload .mp3 File button ', () => {
    const { getByText } = render(<SongRegisterForm />);
    expect(getByText('Upload .mp3 File')).toBeInTheDocument();
  });

  it('renders a Register button type-submit', () => {
    const { getByText } = render(<SongRegisterForm />);
    expect(getByText('Register')).toBeInTheDocument();
  });

  it('validate Song Form TextField and Select, and provides error messages', async () => {
    const { getByTestId, getByText } = render(<SongRegisterForm />);

    await act(async () => {
      fireEvent.change(screen.getByLabelText(/title/i), {
        target: { value: '' }
      });

      fireEvent.change(screen.getByLabelText(/Year Release/i), {
        target: { value: '' }
      });

      fireEvent.change(screen.getByLabelText(/duration/i), {
        target: { value: '' }
      });
    });

    await act(async () => {
      fireEvent.submit(getByTestId('form'));
    });

    expect(getByText('Title is required')).toBeInTheDocument();
    expect(getByText('Year is Required')).toBeInTheDocument();
    expect(getByText('Duration is Required')).toBeInTheDocument();
  });
});
