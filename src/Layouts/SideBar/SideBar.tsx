import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Drawer from '@mui/material/Drawer';
import {
  Box,
  Button,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  ListSubheader
} from '@mui/material';
import HomeIcon from '@mui/icons-material/Home';
import AppRegistrationIcon from '@mui/icons-material/AppRegistration';
import InfoIcon from '@mui/icons-material/Info';
import MenuIcon from '@mui/icons-material/Menu';

interface SideBarProps {
  drawerWidth: number;
}

const pageList = [
  { name: 'Home', route: '/', icon: <HomeIcon /> },
  { name: 'Register', route: '/register', icon: <AppRegistrationIcon /> },
  { name: 'About Us', route: '/about-us', icon: <InfoIcon /> }
];

function SideBar(props: SideBarProps) {
  const navigate = useNavigate();
  const [mobileOpen, setMobileOpen] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState('Home');

  const handleClick = (to: string, selected: string) => {
    navigate(to);
    setSelectedIndex(selected);
    setMobileOpen(false);
  };

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const getDrawerLinks = () => {
    return (
      <>
        <Box px={2} fontWeight={500}>
          <h3>My Tunez</h3>
        </Box>
        <List
          subheader={
            <ListSubheader component="div" sx={{ fontWeight: 'bold', pl: 3 }}>
              Menu
            </ListSubheader>
          }>
          {pageList.map((page) => (
            <ListItem disablePadding key={page.name}>
              <ListItemButton
                selected={selectedIndex === page.name}
                onClick={() => handleClick(page.route, page.name)}>
                <ListItemIcon sx={{ pl: 4 }}>{page.icon}</ListItemIcon>
                <ListItemText primary={page.name} />
              </ListItemButton>
            </ListItem>
          ))}
        </List>
      </>
    );
  };

  return (
    <Box
      component="nav"
      sx={{ width: { sm: props.drawerWidth }, flexShrink: { sm: 0 } }}
      aria-label="mailbox folders">
      <Button onClick={handleDrawerToggle}>
        <MenuIcon />
      </Button>
      <Drawer
        variant="permanent"
        sx={{
          display: { xs: 'none', sm: 'block' },
          '& .MuiDrawer-paper': { boxSizing: 'border-box', width: props.drawerWidth }
        }}
        open>
        {getDrawerLinks()}
      </Drawer>

      <Drawer
        variant="temporary"
        open={mobileOpen}
        onClose={handleDrawerToggle}
        ModalProps={{
          keepMounted: true
        }}
        sx={{
          display: { xs: 'block', sm: 'none' },
          '& .MuiDrawer-paper': { boxSizing: 'border-box', width: props.drawerWidth }
        }}>
        {getDrawerLinks()}
      </Drawer>
    </Box>
  );
}

export default SideBar;
