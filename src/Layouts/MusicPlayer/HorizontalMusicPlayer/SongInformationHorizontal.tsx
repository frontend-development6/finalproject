import React, { useEffect, useState } from 'react'
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { getFile } from '../../../Services/StorageService';

const CoverImage = styled('div')({
    width: 100,
    height: 100,
    objectFit: 'cover',
    overflow: 'hidden',
    flexShrink: 0,
    borderRadius: 8,
    backgroundColor: 'rgba(0,0,0,0.08)',
    '& > img': {
        width: '100%',
    },
});

export default function SongInformationHorizontal({ songData }: { songData: any }) {    
    return (
        <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <CoverImage>
                <img
                    alt={songData? songData.autor: ''}
                    src={songData? songData.image: ''}
                />
            </CoverImage>
            <Box sx={{ ml: 1.5, minWidth: 0 }}>
                <Typography variant="caption" color="text.secondary" fontWeight={500}>
                    {songData? songData.members: ''}
                </Typography>
                <Typography noWrap>
                    <b>{songData? songData.title: ''}</b>
                </Typography>
                <Typography noWrap letterSpacing={-0.25}>
                    {songData? songData.autor:''} &mdash; {songData? songData.album: ''}
                </Typography>
            </Box>
        </Box>
    )
}
