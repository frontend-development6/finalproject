import React, { useEffect, useRef, useState } from 'react'
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Slider from '@mui/material/Slider';
import IconButton from '@mui/material/IconButton';
import Stack from '@mui/material/Stack';
import PauseRounded from '@mui/icons-material/PauseRounded';
import PlayArrowRounded from '@mui/icons-material/PlayArrowRounded';
import FastForwardRounded from '@mui/icons-material/FastForwardRounded';
import FastRewindRounded from '@mui/icons-material/FastRewindRounded';
import VolumeUpRounded from '@mui/icons-material/VolumeUpRounded';
import VolumeDownRounded from '@mui/icons-material/VolumeDownRounded';

import RepeatIcon from '@mui/icons-material/Repeat';
import ShuffleIcon from '@mui/icons-material/Shuffle';
import Grid from '@mui/material/Grid';
import SongInformationVertical from './SongInformationVertical';
import { getFile } from '../../../Services/StorageService';
import { useSelector } from 'react-redux';
import { RootState } from '../../../Services/Store';
import Playlists from '../../../pages/Playlists';
// import { Any } from 'typeorm';


const WallPaper = styled('div')({
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    overflow: 'hidden',
    background: 'linear-gradient(rgb(255, 38, 142) 0%, rgb(255, 105, 79) 100%)',
    transition: 'all 500ms cubic-bezier(0.175, 0.885, 0.32, 1.275) 0s',
    '&:before': {
        content: '""',
        width: '140%',
        height: '140%',
        position: 'absolute',
        top: '-40%',
        right: '-50%',
        background:
            'radial-gradient(at center center, rgb(62, 79, 249) 0%, rgba(62, 79, 249, 0) 64%)',
    },
    '&:after': {
        content: '""',
        width: '140%',
        height: '140%',
        position: 'absolute',
        bottom: '-50%',
        left: '-30%',
        background:
            'radial-gradient(at center center, rgb(247, 237, 225) 0%, rgba(247, 237, 225, 0) 70%)',
        transform: 'rotate(30deg)',
    },
});

const Widget = styled('div')(({ theme }) => ({
    padding: 16,
    borderRadius: 16,
    width: 343,
    maxWidth: '100%',
    margin: 'auto',
    position: 'relative',
    zIndex: 1,
    backgroundColor:
        theme.palette.mode === 'dark' ? 'rgba(0,0,0,0.6)' : 'rgba(255,255,255,0.4)',
    backdropFilter: 'blur(40px)',
}));

const Widget2 = styled('div')(({ theme }) => ({
    padding: 16,
    borderRadius: 16,
    width: '90%',
    maxWidth: '100%',
    margin: 'auto',
    position: 'relative',
    zIndex: 1,
    backgroundColor:
        theme.palette.mode === 'dark' ? 'rgba(0,0,0,0.6)' : 'rgba(255,255,255,0.4)',
    backdropFilter: 'blur(40px)',
}));

const TinyText = styled(Typography)({
    fontSize: '0.75rem',
    opacity: 0.38,
    fontWeight: 500,
    letterSpacing: 0.2,
});
//
const trackList = [
    {
        url: "https://audioplayer.madza.dev/Madza-Chords_of_Life.mp3",
        title: "Madza - Chords of Life",
        tags: ["house"],
    },
    {
        url: "https://audioplayer.madza.dev/Madza-Late_Night_Drive.mp3",
        title: "Madza - Late Night Drive",
        tags: ["dnb"],
    },
    {
        url: "https://audioplayer.madza.dev/Madza-Persistence.mp3",
        title: "Madza - Persistence",
        tags: ["dubstep"],
    },
];
//

export default function MusicPlayerVertical() {

    const [song, setSong] = useState("");
    let playlist = useSelector((state: RootState) => {
        return state.reproduction.list;
    });

    const theme = useTheme();
    const [paused, setPaused] = React.useState(true);

    const mainIconColor = theme.palette.mode === 'dark' ? '#fff' : '#000';
    const lightIconColor =
        theme.palette.mode === 'dark' ? 'rgba(255,255,255,0.4)' : 'rgba(0,0,0,0.4)';
    let autoPlayNextTrack = true;

    const [audio, setAudio] = useState<any | null>(null);
    const [title, setTitle] = useState("");
    const [length, setLength] = useState(0);
    const [time, setTime] = useState(0);
    const [slider, setSlider] = useState<any>(1);
    const [drag, setDrag] = useState(0);
    const [volume, setVolume] = useState<number>(10);
    let [end, setEnd] = useState(0);
    const [shuffled, setShuffled] = useState(false);
    const [looped, setLooped] = useState(false);

    let [songData, setSongData] = useState<PlayerSong>();

    let [curTrack, setCurTrack] = useState(0);


    const fmtMSS = (s: any) => new Date(1000 * s).toISOString().substr(15, 4);

    /* props.pausedTrack(paused); */
    useEffect(() => {
        console.log("current track now:", curTrack)

        let source = playlist[curTrack] && playlist[curTrack].file ? playlist[curTrack].file as string : undefined;
        const audio = new Audio(source);

        const setAudioData = () => {
            setLength(audio.duration);
            setTime(audio.currentTime);
        };

        const setAudioTime = () => {
            const curTime = audio.currentTime;
            setTime(curTime);
            setSlider(curTime ? ((curTime * 100) / audio.duration).toFixed(1) : 0);
        };

        const setAudioEnd = () => setEnd((end += 1));

        audio.addEventListener("loadeddata", setAudioData);
        audio.addEventListener("timeupdate", setAudioTime);

        const setAudioVolume = () => setVolume(audio.volume);
        audio.addEventListener("volumechange", setAudioVolume);
        audio.addEventListener("ended", setAudioEnd);



        setAudio(audio);
        /* props.audioTrack(audio); */
        setTitle(playlist[curTrack] && playlist[curTrack].title ? playlist[curTrack].title : '');
        return () => {
            audio.pause();
        };
    }, []);

    useEffect(() => {
        if (audio != null) {
            audio.volume = volume;
        }
    }, [volume]);

    const loop = () => {
        setLooped(!looped);
    };

    const shuffle = () => {
        setShuffled(!shuffled);
    };

    const shufflePlaylist = (arr: any): any => {
        if (arr.length === 1) return arr;
        const rand = Math.floor(Math.random() * arr.length);
        return [arr[rand], ...shufflePlaylist(arr.filter((_: any, i: any) => i != rand))];
    };

    const isInitialMount = useRef(true);
    useEffect(() => {
        if (isInitialMount.current) {
            isInitialMount.current = false;
        } else {
            if (shuffled) {
                playlist = shufflePlaylist(playlist);
            }
            !looped && autoPlayNextTrack ? next() : play();
        }
    }, [end]);

    useEffect(() => {
        if (audio != null) {
            pause();
            const val = Math.round((drag * audio.duration) / 100);
            audio.currentTime = val;
        }
    }, [drag]);

    function valueSound(value: number) {
        setVolume(value / 100);
        return `${value}`;
    }


    const play = () => {
        audio.play();
    };

    const pause = () => {
        audio.pause();
    };

    useEffect(() => {
        if (audio != null) {
            audio.src = playlist[curTrack] && playlist[curTrack].file ? playlist[curTrack].file : null;
            setTitle(playlist[curTrack] && playlist[curTrack].title ? playlist[curTrack].title : '');
            setSongData(playlist[curTrack]);
            /* props.currentTrack(curTrack); */
            if (!paused) {
                play();
            }
        }
    }, [curTrack]);

    const previous = () => {
        const index = curTrack;
        index !== 0
            ? setCurTrack((index - 1))
            : setCurTrack((playlist.length - 1));
    };

    const next = () => {
        const index = curTrack;
        (index !== playlist.length - 1)
            ? setCurTrack((index + 1))
            : setCurTrack((0));
    };

    const handleClick = (event: any, key: any) => {
        const index = playlist.indexOf(key);
        setCurTrack((index));
        play();
    };

    return (
        <>
            <Box sx={{ width: '100%', overflow: 'hidden' }}>
                <Widget>
                    <Box sx={{ margin: '5%' }}>
                        <SongInformationVertical songData={songData} />
                        <Grid>
                            <input
                                type="range"
                                min="1"
                                max="100"
                                step="1"
                                value={slider}
                                onChange={(e: any) => {
                                    setSlider(e.target.value);
                                    setDrag(e.target.value);
                                }}
                                onMouseUp={play}
                                onTouchEnd={play}
                                style={{
                                    background: `linear-gradient(90deg, var(--progressUsed) ${Math.floor(
                                        slider
                                    )}%, var(--progressLeft) ${Math.floor(slider)}%)`,
                                    width: '100%',
                                }}
                            />
                            <Box
                                sx={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                }}
                            >
                                <TinyText>{`${!time ? "0:00" : fmtMSS(time)}`}</TinyText>
                                <TinyText>{`${!length ? "0:00" : fmtMSS(length)}`}</TinyText>
                            </Box>
                        </Grid>
                        <Box
                            sx={{
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'center',
                                mt: -1,
                            }}
                        >
                            <IconButton aria-label="repeat song" onClick={() => setLooped(!looped)}>
                                {looped ? (
                                    <RepeatIcon htmlColor={mainIconColor} onClick={loop} />)
                                    :
                                    (<RepeatIcon htmlColor={lightIconColor} onClick={loop} />)}
                            </IconButton>
                            <IconButton aria-label="previous song">
                                <FastRewindRounded fontSize="large" htmlColor={mainIconColor} onClick={previous} />
                            </IconButton>
                            <IconButton
                                aria-label={paused ? 'play' : 'pause'}
                                onClick={() => setPaused(!paused)}
                            >
                                {paused ? (
                                    <PlayArrowRounded
                                        sx={{ fontSize: '3rem' }}
                                        htmlColor={mainIconColor}
                                        onClick={play} />
                                ) : (
                                    <PauseRounded sx={{ fontSize: '3rem' }} htmlColor={mainIconColor} onClick={pause} />
                                )}
                            </IconButton>
                            <IconButton aria-label="next song">
                                <FastForwardRounded fontSize="large" htmlColor={mainIconColor} onClick={next} />
                            </IconButton>
                            <IconButton aria-label="shuffle song" onClick={() => setShuffled(!shuffled)}>
                                {shuffled ? (
                                    <ShuffleIcon htmlColor={mainIconColor} onClick={shuffle} />)
                                    : (
                                        <ShuffleIcon htmlColor={lightIconColor} onClick={shuffle} />
                                    )
                                }
                            </IconButton>
                        </Box>
                        <Stack spacing={2} direction="row" sx={{ mb: 1, px: 1 }} alignItems="center">
                            <VolumeDownRounded htmlColor={lightIconColor} />
                            <Slider
                                aria-label="Volume"
                                defaultValue={volume}
                                getAriaValueText={valueSound}
                                valueLabelDisplay="auto"
                                sx={{
                                    color: theme.palette.mode === 'dark' ? '#fff' : 'rgba(0,0,0,0.87)',
                                    '& .MuiSlider-track': {
                                        border: 'none',
                                    },
                                    '& .MuiSlider-thumb': {
                                        width: 24,
                                        height: 24,
                                        backgroundColor: '#fff',
                                        '&:before': {
                                            boxShadow: '0 4px 8px rgba(0,0,0,0.4)',
                                        },
                                        '&:hover, &.Mui-focusVisible, &.Mui-active': {
                                            boxShadow: 'none',
                                        },
                                    },
                                }} />
                            <VolumeUpRounded htmlColor={lightIconColor} />
                        </Stack>
                    </Box>
                </Widget>
                {/* <WallPaper /> */}
            </Box>

        </>
    )
}
