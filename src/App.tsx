import { Box, createTheme, CssBaseline, ThemeProvider } from '@mui/material';
import React, { useState } from 'react';
import './App.css';
import NavBar from './Components/NavBar';
import RoutesMT from './Routes/RoutesMT';
import { useSelector } from 'react-redux';
import { RootState } from './Services/Store';

function App() {

  const baseTheme = createTheme({
    typography: {
      fontFamily: "Gill Sans, Gill Sans MT, Calibri, sans-serif",
      fontSize: 16
    }
  })

  const darkTheme = createTheme({
    ...baseTheme,
    components: {
      MuiCssBaseline: {
        styleOverrides: {
          body: {
            backgroundColor: '#0f2027',
            backgroundImage: `linear-gradient(to right, #0f2027, #203a43, #2c5364)`,
          },
        },
      },
      MuiAppBar: {
        styleOverrides: {
          colorPrimary: {
            opacity: "100%"
          }
        }
      },
      MuiPaper:{
        styleOverrides: {
          root: {
            backgroundColor: "rgba(0,0,0,0.6)",
            backdropFilter: 'blur(40px)',
        },
      }
    }},
    palette: {
      mode: "dark",
      primary: {
        main: "#c1eaf2",
      },
      secondary: {
        main: "#62a388"
      },
      action: {
        hover: "#2c5d63"
      },
    },
  })
  const lightTheme = createTheme({
    ...baseTheme,
    components: {
      MuiCssBaseline: {
        styleOverrides: {
          body: {
            backgroundColor: '#be93c5',
            backgroundImage: `linear-gradient(to right, #be93c5, #7bc6cc)`,
          },
        },
      },
      MuiPaper:{
        styleOverrides: {
          root: {
            backgroundColor: "rgba(255,255,255,0.4)",
            backdropFilter: 'blur(40px)',
        },
      }
    }},
    palette: {
      mode: "light",
      primary: {
        main: "#38598b"
      },
      action: {
        hover: "#9fd3c7"
      },
      secondary: {
        main: "#77628c"
      },
      // background: {
      //   paper: "rgba(255,255,255,0.4)",
      // }
    }
  })

  const pages = [
    { name: 'My Music', route: '/' },
    { name: 'Playlists', route: '/playlist' },
    { name: 'About Us', route: '/about-us' }
    
  ];
  
  const theme = useSelector((state: RootState) => (state.theme.mode == 'dark' ? darkTheme : lightTheme))

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <div className="App">
        <NavBar pages={pages}></NavBar>
        <Box>
          <RoutesMT />
        </Box>
      </div>
    </ThemeProvider>
  );
}

export default App;
