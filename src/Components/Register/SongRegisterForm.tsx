import React, { useEffect, useState } from 'react';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import MenuItem from '@mui/material/MenuItem';
import SendIcon from '@mui/icons-material/Send';
import { getArtists, saveMp3File, saveSong, getArtistAlbums } from '../../Services/StorageService';
import { Stack, Alert, Box } from '@mui/material';
import UploadFileIcon from '@mui/icons-material/UploadFile';
import { genres } from '../../Models/Genre';
import { useFormik } from 'formik';
import * as Yup from 'yup';

function SongRegisterForm() {
  const [artists, setArtists] = useState<Artist[]>([]);
  const [albums, setAlbums] = useState<Album[]>([]);
  const [stateMessage, setStateMessage] = useState(false);
  const [file, setFile] = useState('');

  const validationSchema = Yup.object({
    title: Yup.string().min(3, '3 characters minimum').required('Title is required'),
    genre: Yup.string().required('Genre is Required'),
    releaseYear: Yup.number()
      .required('Year is Required')
      .min(1800, 'The year must be over 1800'),
    artist: Yup.string().required('Artist is Required'),
    album: Yup.string().required('Album is Required'),
    duration: Yup.string().required('Duration is Required')
  });

  const formik = useFormik({
    initialValues: {
      title: '',
      genre: '',
      releaseYear: 0,
      artist: '',
      album: '',
      duration: ''
    },
    validationSchema: validationSchema,
    onSubmit: (data: any) => {
      let song: Song = {
        id: 0,
        title: data.title,
        genre: data.genre,
        releaseYear: data.releaseYear,
        artist: data.artist,
        album: data.album,
        duration: data.duration,
        file: file,
		inPlayList: false
      };
      save(song);
    }
  });

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    let file: File | null = null;
    if (event.target.files && event.target.files.length > 0) {
      file = event.target.files[0];
    }
    if (file) {
      const fileName = file.name;
      setFile(fileName);
      const reader = new FileReader();
      reader.onload = () => {
        const file = reader.result;
        saveMp3File(file, fileName);
      };
      reader.readAsDataURL(file);
    }
  };

  useEffect(() => {
    getArtists().then((response) => {
      if (response) {
        setArtists(response);
      }
    });
  }, []);

  const save = (song: Song) => {
    saveSong(song).then((response) => {
      console.log(response);
      showSuccessMessage();
    });
  };

  const showSuccessMessage = () => {
    setStateMessage(true);
    setTimeout(() => {
      setStateMessage(false);
    }, 2000);
    resetState();
  };

  const resetState = () => {
    setFile('');
    formik.resetForm();
  };

  const handleChange = (e: any) => {
    getArtistAlbums(e.target.value).then((response: Album[]) => {
      setAlbums(response);
    });

    formik.values.album = '';
    formik.handleChange(e);
  };

  return (
    <>
      {stateMessage ? (
        <Stack sx={{ width: '100%' }} spacing={2}>
          <Alert severity="success">Song Saved!!!</Alert>
        </Stack>
      ) : (
        <></>
      )}
      <Box>
        <form onSubmit={formik.handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="titleSong"
                name="title"
                label="Title:"
                value={formik.values.title}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                size="small"
                margin="normal"
                fullWidth
                error={formik.touched.title && Boolean(formik.errors.title)}
                helperText={formik.touched.title && formik.errors.title}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="selectGenres"
                select
                name="genre"
                label="Genres"
                value={formik.values.genre}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                fullWidth
                size="small"
                margin="normal"
                error={formik.touched.genre && Boolean(formik.errors.genre)}
                helperText={formik.touched.genre && formik.errors.genre}>
                {genres.map((genre) => (
                  <MenuItem key={genre} value={genre}>
                    {genre}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="yearReleaseSongAlbum"
                label="Year Release:"
                name="releaseYear"
                value={formik.values.releaseYear}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                type="number"
                size="small"
                margin="normal"
                fullWidth
                error={formik.touched.releaseYear && Boolean(formik.errors.releaseYear)}
                helperText={formik.touched.releaseYear && formik.errors.releaseYear}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="selectArtist"
                select
                label="Artist"
                name="artist"
                value={formik.values.artist}
                onChange={handleChange}
                onBlur={formik.handleBlur}
                fullWidth
                size="small"
                margin="normal"
                error={formik.touched.artist && Boolean(formik.errors.artist)}
                helperText={formik.touched.artist && formik.errors.artist}>
                {artists.map((artist) => (
                  <MenuItem key={artist.id} value={artist.id}>
                    {artist.name}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="selectAlbum"
                select
                label="Album"
                name="album"
                value={formik.values.album}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                fullWidth
                size="small"
                margin="normal"
                error={formik.touched.album && Boolean(formik.errors.album)}
                helperText={formik.touched.album && formik.errors.album}>
                {albums.map((album) => (
                  <MenuItem key={album.id} value={album.id}>
                    {album.title}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="durationSong"
                label="Duration:"
                type="text"
                value={formik.values.duration}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                name="duration"
                size="small"
                margin="normal"
                fullWidth
                error={formik.touched.duration && Boolean(formik.errors.duration)}
                helperText={formik.touched.duration && formik.errors.duration}
              />
            </Grid>
            <Grid item xs={12} display="flex">
              <Button
                variant="contained"
                component="label"
                size="small"
                endIcon={<UploadFileIcon />}>
                Upload .mp3 File
                <input accept="audio/*" onChange={handleFileChange} type="file" hidden />
              </Button>
              <Box marginX={2}>{file}</Box>
            </Grid>
            <Grid item xs={12}>
              <Button
                variant="contained"
                size="small"
                color="success"
                endIcon={<SendIcon />}
                type="submit"
                disabled={!(formik.isValid && formik.dirty && file != '')}>
                Register
              </Button>
            </Grid>
          </Grid>
        </form>
      </Box>
    </>
  );
}

export default SongRegisterForm;
