import React, { useState, useEffect } from 'react';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import MenuItem from '@mui/material/MenuItem';
import SendIcon from '@mui/icons-material/Send';
import { saveImage, saveAlbum, getArtists } from '../../Services/StorageService';
import { genres } from '../../Models/Genre';
import { Stack, Alert, Box } from '@mui/material';
import UploadFileIcon from '@mui/icons-material/UploadFile';
import { useFormik } from 'formik';
import * as Yup from 'yup';

function AlbumRegisterForm() {
  const [artists, setArtists] = useState<Artist[]>([]);
  const [stateMessage, setStateMessage] = useState(false);
  const [image, setImage] = useState('');

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    let file: File | null = null;
    if (event.target.files && event.target.files.length > 0) {
      file = event.target.files[0];
    }

    if (file) {
      const fileName = file.name;

      setImage(fileName);

      const reader = new FileReader();
      reader.onload = () => {
        const image = reader.result;
        saveImage(image, fileName);
      };

      reader.readAsDataURL(file);
    }
  };

  useEffect(() => {
    getArtists().then((response) => {
      if (response) {
        setArtists(response);
      }
    });
  }, []);

  const save = (album: Album) => {
    saveAlbum(album).then((albums: Album[]) => {
      console.log('albums: ', albums);
      showSuccessMessage();
    });
  };

  const showSuccessMessage = () => {
    setStateMessage(true);
    setTimeout(() => {
      setStateMessage(false);
    }, 2000);
    resetState();
  };

  const resetState = () => {
    setImage('');
    formik.resetForm();
  };

  const validationSchema = Yup.object({
    title: Yup.string().min(3, '3 characters minimum').required('Title is required'),
    year: Yup.number().required('Year is Required').min(1800, 'The year must be over 1800'),
    genre: Yup.string().required('Genre is Required'),
    artist: Yup.string().required('Artist is Required')
  });
  const formik = useFormik({
    initialValues: {
      title: '',
      year: 0,
      genre: '',
      artist: ''
    },
    validationSchema: validationSchema,
    onSubmit: (data: any) => {
      console.log('formulario', data);
      let album: Album = {
        id: 0,
        title: data.title,
        year: data.year,
        genre: data.genre,
        artist: data.artist,
        image: image
      };
      save(album);
    }
  });

  return (
    <>
      {stateMessage ? (
        <Stack sx={{ width: '100%' }} spacing={2}>
          <Alert severity="success">Album Saved!!!</Alert>
        </Stack>
      ) : (
        <></>
      )}
      <Box>
        <form onSubmit={formik.handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12} md={6}>
              <TextField
                required
                id="title"
                name="title"
                value={formik.values.title}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                label="Title:"
                size="small"
                margin="normal"
                fullWidth
                error={formik.touched.title && Boolean(formik.errors.title)}
                helperText={formik.touched.title && formik.errors.title}
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField
                required
                id="year"
                name="year"
                value={formik.values.year}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                label="Year Release:"
                type="number"
                size="small"
                margin="normal"
                fullWidth
                error={formik.touched.year && Boolean(formik.errors.year)}
                helperText={formik.touched.year && formik.errors.year}
              />
            </Grid>
          </Grid>

          <Grid container spacing={2}>
            <Grid item xs={12} md={6}>
              <TextField
                required
                id="genre"
                name="genre"
                select
                label="Genre"
                value={formik.values.genre}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                fullWidth
                size="small"
                margin="normal"
                error={formik.touched.genre && Boolean(formik.errors.genre)}
                helperText={formik.touched.genre && formik.errors.genre}>
                {genres.map((option) => (
                  <MenuItem key={option} value={option}>
                    {option}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField
                required
                id="selectArtist"
                select
                label="Artist"
                name="artist"
                value={formik.values.artist}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                fullWidth
                size="small"
                margin="normal"
                error={formik.touched.artist && Boolean(formik.errors.artist)}
                helperText={formik.touched.artist && formik.errors.artist}>
                {artists.map((artist) => (
                  <MenuItem key={artist.id} value={artist.id}>
                    {artist.name}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={12} display="flex">
              <Button
                variant="contained"
                component="label"
                size="small"
                endIcon={<UploadFileIcon />}>
                Upload File
                <input accept="image/*" onChange={handleFileChange} type="file" hidden />
              </Button>
              <Box marginX={2}>{image}</Box>
            </Grid>
            <Grid item xs={12}>
              <Button
                variant="contained"
                size="small"
                color="success"
                endIcon={<SendIcon />}
                type="submit"
                disabled={!(formik.isValid && formik.dirty && image != '')}>
                Register
              </Button>
            </Grid>
          </Grid>
        </form>
      </Box>
    </>
  );
}

export default AlbumRegisterForm;
