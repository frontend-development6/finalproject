import React, { useState } from 'react';
import {
  Button,
  Box,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  OutlinedInput,
  Select,
  Divider,
  DialogTitle,
  TextField,
  Stack,
  Alert
} from '@mui/material';
import UploadFileIcon from '@mui/icons-material/UploadFile';
import SendIcon from '@mui/icons-material/Send';
import { saveImage, saveArtist } from '../../Services/StorageService';
import { genres } from '../../Models/Genre';
import { useFormik } from 'formik';
import { useDispatch } from 'react-redux';
import { updateArtists } from '../../Services/Reducers/artistsSlice';
import * as Yup from 'yup';

function ArtistRegisterForm() {
  const dispatch = useDispatch();
  const [stateMessage, setStateMessage] = useState(false);
  const [image, setImage] = useState('');

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    let file: File | null = null;
    if (event.target.files && event.target.files.length > 0) {
      file = event.target.files[0];
    }

    if (file) {
      const fileName = file.name;

      setImage(fileName);

      const reader = new FileReader();
      reader.onload = () => {
        const image = reader.result;
        saveImage(image, fileName);
      };

      reader.readAsDataURL(file);
    }
  };

  const save = (artist: Artist) => {
    saveArtist(artist).then((artists: Artist[]) => {
      dispatch(updateArtists(artists));
      showSuccessMessage();
    });
  };

  const showSuccessMessage = () => {
    setStateMessage(true);
    setTimeout(() => {
      setStateMessage(false);
    }, 2000);
    resetState();
  };

  const resetState = () => {
    setImage('');
    formik.resetForm();
  };

  const validationSchema = Yup.object({
    name: Yup.string().required('Name is required'),
    members: Yup.string().required('At leastone member is required'),
    genres: Yup.array().required('Genres are Required'),
    webSite: Yup.string().url().required('Web Site is Required')
  });

  const formik = useFormik({
    initialValues: {
      name: '',
      members: '',
      genres: [],
      webSite: ''
    },
    validationSchema: validationSchema,
    onSubmit: (data: any) => {
      let artist: Artist = {
        id: 0,
        name: data.name,
        genres: data.genres,
        members: data.members,
        webSite: data.webSite,
        image: image
      };
      save(artist);
    }
  });
  return (
    <>
      {stateMessage ? (
        <Stack sx={{ width: '100%' }} spacing={2}>
          <Alert severity="success">Artist Saved!!!</Alert>
        </Stack>
      ) : (
        <></>
      )}
      <Box>
        <form onSubmit={formik.handleSubmit} data-testid='form'>
          <Grid container spacing={2}>
            <Grid item xs={12} md={6}>
              <TextField
                id="name"
                label="Name"
                name="name"
                size="small"
                margin="normal"
                fullWidth
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.name}
                error={formik.touched.name && Boolean(formik.errors.name)}
                helperText={formik.touched.name && formik.errors.name}
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <FormControl sx={{ width: '100%', marginTop: '16px' }} size="small">
                <InputLabel id="genresLabel">Genres</InputLabel>
                <Select
                  labelId="genresLabel"
                  id="genres"
                  name="genres"
                  multiple
                  value={formik.values.genres}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  input={<OutlinedInput label="Genres" />}
                  renderValue={(selected) => selected.join(', ')}>
                  {genres.map((genre) => (
                    <MenuItem key={genre} value={genre}>
                      {genre}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <TextField
                multiline
                id="members"
                name="members"
                label="Members"
                size="small"
                margin="normal"
                value={formik.values.members}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                fullWidth
                error={formik.touched.members && Boolean(formik.errors.members)}
                helperText={formik.touched.members && formik.errors.members}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="webSite"
                label="Web Site"
                name="webSite"
                size="small"
                margin="normal"
                fullWidth
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.webSite}
                error={formik.touched.webSite && Boolean(formik.errors.webSite)}
                helperText={formik.touched.webSite && formik.errors.webSite}
              />
            </Grid>
            <Grid item xs={12} display="flex">
              <Button
                variant="contained"
                component="label"
                size="small"
                endIcon={<UploadFileIcon />}>
                Upload File
                <input accept="image/*" onChange={handleFileChange} type="file" hidden />
              </Button>
              <Box marginX={2}>{image}</Box>
            </Grid>
            <Divider color="white" />
            <Grid item xs={12}>
              <Button
                disabled={!(formik.isValid && formik.dirty && image != '')}
                type="submit"
                variant="contained"
                color="success"
                endIcon={<SendIcon />}
                size="small">
                Register
              </Button>
            </Grid>
          </Grid>
        </form>
      </Box>
    </>
  );
}

export default ArtistRegisterForm;
