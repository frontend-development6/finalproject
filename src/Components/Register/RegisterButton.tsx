import React, { useState } from 'react';
import { Dialog, DialogTitle, DialogContent, Box, Tabs, Tab, Fab } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import AlbumRegisterForm from './AlbumRegisterForm';
import ArtistRegisterForm from './ArtistRegisterForm';
import SongRegisterForm from './SongRegisterForm';

function RegisterButton() {
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(0);

  const handleOpenClose = () => {
    setOpen(!open);
  };

  const handleTabChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  return (
    <>
      <Fab size="small" color="success" aria-label="add" onClick={handleOpenClose}>
        <AddIcon />
      </Fab>
      <Dialog onClose={handleOpenClose} open={open} maxWidth={'md'} fullWidth={true}>
        <DialogTitle sx={{ textAlign: 'center' }}>Register</DialogTitle>
        <DialogContent dividers>
          <Box sx={{ width: '100%' }} display="flex" flexGrow={1}>
            <Box>
              <Tabs value={value} onChange={handleTabChange} orientation="vertical">
                <Tab label="Artist" value={0} />
                <Tab label="Album" value={1} />
                <Tab label="Song" value={2} />
              </Tabs>
            </Box>
			<Box flexGrow={1} p={2}>
				{value == 0 ?	
					<ArtistRegisterForm />	
					:<></>
				}
				{value == 1 ?
					<AlbumRegisterForm />
					:<></>
				}
				{value == 2 ?
					<SongRegisterForm />
					:<></>
				}
			</Box>
          </Box>
        </DialogContent>
      </Dialog>
    </>
  );
}

export default RegisterButton;
