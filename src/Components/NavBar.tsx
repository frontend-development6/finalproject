import React, { useState } from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Button from '@mui/material/Button';
import { useNavigate } from 'react-router-dom';
import { Grid, Fab } from '@mui/material';
import RegisterButton from './Register/RegisterButton';
import NightsStayIcon from '@mui/icons-material/NightsStay';
import WbSunnyIcon from '@mui/icons-material/WbSunny';
import { useDispatch } from 'react-redux';
import { darkMode, lightMode } from '../Services/Reducers/themeSlice';

export interface NavBarPage {
  name: string;
  route: string;
}

interface NavBarProps {
  pages: NavBarPage[];
}

function NavBar({ pages }: NavBarProps) {
  const dispatch = useDispatch();
  const [themeState, setThemeState] = useState(false);
  const navigate = useNavigate();
  const handlePageSelection = (page: NavBarPage) => {
    navigate(page.route);
  };

  const handleClick = () => {
    const newTheme = themeState ? 'dark' : 'light';
    if (!themeState) {
      dispatch(lightMode());
    } else {
      dispatch(darkMode());
    }
    setThemeState(!themeState);
  };

  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <Grid container>
            <Grid item xs={2}>
              <div>My Tunez</div>
            </Grid>
            <Grid item xs={8} display="flex" justifyContent="center">
              {pages.map((page) => (
                <Button
                  key={page.name}
                  color="inherit"
                  onClick={() => {
                    handlePageSelection(page);
                  }}>
                  {page.name}
                </Button>
              ))}
            </Grid>
            <Grid item xs={2} display="flex" justifyContent="end">
              <RegisterButton />
              <Fab
                size="small"
                color={themeState ? 'info' : 'warning'}
                aria-label="mode"
                onClick={handleClick}
                sx={{ marginLeft: 1 }}>
                {themeState ? <NightsStayIcon /> : <WbSunnyIcon />}
              </Fab>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default NavBar;
