import React from 'react'
import IconButton from '@mui/material/IconButton';
import {Grid, Link, Card, Box, CardContent, Typography, CardMedia} from "@mui/material"

function CardPlaylists() {

    const CardMediaWidget = {
        boxShadow: '5px 10px 20px 5px Green inset',
        borderRadius: '100px'
    }
  return (
    <Grid container>
        <Grid item>
            <Card sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }} >
            <Grid style={CardMediaWidget}>
            <CardMedia
                component="img"
                sx={{ width: '300px' , height: '300px'}}
                image="https://elcomercio.pe/resizer/Q2zLEdhgIf_D2KPUp4ETdVoe49A=/580x330/smart/filters:format(jpeg):quality(75)/cloudfront-us-east-1.images.arcpublishing.com/elcomercio/PWY2QA6P4VHAPNRGJRPJXA3KGM.jpg"
                alt="PlayList"
            />
            </Grid>
            <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                <CardContent sx={{ flex: '1 0 auto', display: 'flex', alignItems: 'center',  flexDirection: 'column' }}>
                    <Typography component="div" variant="h5">
                        Pop
                    </Typography>
                    <Typography variant="subtitle1" color="text.secondary" component="div">
                        by My Tunez
                    </Typography>
                </CardContent>
            </Box>
            </Card>
        </Grid>
    </Grid>
  )
}

export default CardPlaylists