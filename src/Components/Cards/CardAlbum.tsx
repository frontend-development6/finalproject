import React from 'react';
import { useTheme } from '@mui/material/styles';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import Container from '@mui/material/Container';
import { useEffect, useState } from 'react';
import { getFile, getPlayerSongsByAlbum } from '../../Services/StorageService';

import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { Divider, Box, Paper } from '@mui/material';
import { useDispatch } from 'react-redux';
import { addElement, deleteElement } from '../../Services/Reducers/playListSlice';
import IconButton from '@mui/material/IconButton';
import PauseRounded from '@mui/icons-material/PauseRounded';
import PlayArrowRounded from '@mui/icons-material/PlayArrowRounded';

import { setSongPlaylistState } from '../../Services/StorageService';
import PlaylistButton from './PlaylistButton';

interface CardAlbumProps {
  album: Album;
  currentTrack: number;
  pausedTrack: boolean;
  handlePlayerSongList(list: PlayerSong[], albumId: number): void;
}

export default function CardAlbum(props: any) {
  const theme = useTheme();
  const mainIconColor = theme.palette.mode === 'dark' ? '#fff' : '#000';

  const dispatch = useDispatch();
  const [checked, setChecked] = React.useState([0]);
  const [img, setImg] = useState('');
  const [songList, setSongs] = useState<PlayerSong[]>([]);
  let listSended = false;

  useEffect(() => {
    getFile(props.album.image).then((img_src) => {
      if (img_src) {
        setImg(img_src);
      }
    });

    getPlayerSongsByAlbum(props.album.id).then((songList: PlayerSong[]) => {
      setSongs(songList);

      if (!listSended) {
        listSended = true;
        props.handlePlayerSongList(songList, props.album.id);
      }
    });
  }, []);

  const handleSelectToPlayList = (song: PlayerSong, isAdding: boolean) => {
    if (isAdding) {
      dispatch(deleteElement(song));
      setSongPlaylistState(song.songId, false);
    } else {
      dispatch(addElement(song));
      setSongPlaylistState(song.songId, true);
    }
  };

  const handleToggle = (value: number) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  const play = () => {
    props.audioTrack.play();
  };

  const pause = () => {
    props.audioTrack.pause();
  };

  const handleClick = (event: any, key: any) => {
    props.currentTrack((key));
    play();
  };

  return (
    <Container maxWidth="md" sx={{ margin: '2%' }}>
      <Paper sx={{ display: 'flex', flexDirection: 'column', padding: '5px' }} elevation={3}>
        <Box
          paddingX={2}
          pt={2}
          display="flex"
          justifyContent={'space-between'}
          alignItems={'center'}>
          <Typography component="div" variant="h5">
            {props.album.title} - {props.album.artist}
          </Typography>
          <div>{props.album.year}</div>
        </Box>
        <Divider />
        <Box display="flex">
          <CardMedia
            component="img"
            sx={{ maxWidth: 150, maxHeight: 150, margin: '2%' }}
            image={img}
            alt="Album image"
          />
          <Box sx={{ flexDirection: 'column', width: '100%' }}>
            <List sx={{ width: '100%' }}>
              {songList
                .sort((a, b) => (a.title > b.title ? 1 : -1))
                .map((el, index) => {
                  return (
                    <ListItem key={index} disablePadding>
                      {props.currentTrack === index ? (
                        <>
                          <ListItemButton role={undefined} dense>
                            <IconButton
                              aria-label={props.pausedTrack ? 'play' : 'pause'}
                              onClick={() => props.setPausedTrack(!props.pausedTrack)}>
                              {props.pausedTrack ? (
                                <PlayArrowRounded
                                  sx={{ fontSize: '3rem' }}
                                  htmlColor={mainIconColor}
                                  onClick={play}
                                />
                              ) : (
                                <PauseRounded
                                  sx={{ fontSize: '3rem' }}
                                  htmlColor={mainIconColor}
                                  onClick={pause}
                                />
                              )}
                            </IconButton>
                            <ListItemText primary={`${el.title}`} />
                          </ListItemButton>
                          <ListItemIcon>
                            <PlaylistButton
                              song={el}
                              handleSelected={handleSelectToPlayList}
                            />
                          </ListItemIcon>
                        </>
                      ) : (
                        <>
                          <ListItemButton role={undefined} dense>
                            <ListItemIcon>
                              <PlayArrowIcon onClick={(event) => handleClick(event, index)} />
                            </ListItemIcon>
                            <ListItemText primary={`${el.title}`} />
                          </ListItemButton>
                          <ListItemIcon>
                            <PlaylistButton
                              song={el}
                              handleSelected={handleSelectToPlayList}
                            />
                          </ListItemIcon>
                        </>
                      )}
                    </ListItem>
                  );
                })}
            </List>
          </Box>
        </Box>
      </Paper>
    </Container>
  );
}
