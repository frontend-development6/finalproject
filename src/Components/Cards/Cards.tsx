import * as React from "react";
import { Grid, Link, Card, CardContent, Typography, Box, IconButton } from "@mui/material";
import CardsSongs from "./CardsSongs";
import { useSelector } from "react-redux";
import { RootState } from "../../Services/Store";
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import { useDispatch } from 'react-redux'; 
import { deleteElement } from '../../Services/Reducers/playListSlice';
import { setSongPlaylistState } from "../../Services/StorageService";


const Cards = (props: any) => {
    
    const playlist = useSelector((state: RootState) => state.playlist.list);
    const containerLinkHead = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    };

    const cardsContainer = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        margin: '50px'
    };
    const dispatch = useDispatch();

    const handleSelectToPlayList = (song: PlayerSong) => {
        dispatch(deleteElement(song));
        setSongPlaylistState(song.songId, false);
    }

    return (
    <div style={cardsContainer}>
        <Grid container direction="row" spacing={3}>
            <Grid item xs={12} sm={12} md={12}>
                <Grid container >
                    <Grid item xs={10} sm={10} md={10} >
                    <h1>My Playlist</h1>
                    </Grid>
                    <Grid item xs={2} sm={2} md={2} style={containerLinkHead}>
                        <h4>
                        <Link href="#" underline="hover">
                            {'See all'}
                        </Link></h4>
                    </Grid>
                    <Grid item xs={12}>
                            <Grid container sx={{height: '50px', width: '88vw', borderBottom: 'solid 1px ', 
                                                display: 'flex', alignItems: 'center'}}>
                                <Grid item xs={2} sm={2} md={2}>
                                    <CardContent>#</CardContent>
                                </Grid>     
                                <Grid item xs={4} sm={4} md={4}>
                                    <CardContent sx={{ flex: '1 0 auto' , paddingTop:'4%'}}>
                                        <Typography component="div" variant="subtitle1" fontWeight={700}>
                                            Title
                                        </Typography>
                                    </CardContent>
                                </Grid> 
                                <Grid item xs={2} sm={2} md={2}>
                                    <CardContent sx={{ flex: '1 0 auto' , paddingTop:'4%'}}>
                                        <Typography component="div" variant="subtitle1" fontWeight={700}>
                                            Genre
                                        </Typography>
                                    </CardContent>
                                </Grid>       
                                <Grid item xs={4} sm={4} md={4}>
                                    <Box sx={{ display: 'flex', alignItems: 'center', pl: 1, pb: 1 , justifyContent: 'center', height: '100%'}}>
                                        <IconButton aria-label="play/pause">
                                            <AccessTimeIcon sx={{ height: 30, width: 30 }} />
                                        </IconButton>
                                    </Box>
                                </Grid> 
                            </Grid>
                    </Grid>
                </Grid>
                {playlist.map((playerSong: any) => (
                    <CardsSongs song={playerSong} key={playerSong} handleRemove={handleSelectToPlayList}></CardsSongs>
                ))}
            </Grid>
        </Grid>
    </div>
    );
}

export default Cards;


