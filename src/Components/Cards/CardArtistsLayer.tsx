import React from 'react'
import CardsArtist from "./CardsArtist";
import { useEffect, useState } from "react";
import { getArtists } from '../../Services/StorageService';
import { Grid, Link, Typography } from "@mui/material";
import GenreMenu from '../GenreMenu';
import { useSelector } from 'react-redux';
import { RootState } from '../../Services/Store';
import { useDispatch } from 'react-redux';
import { updateArtists } from '../../Services/Reducers/artistsSlice';

interface ArtistProps {
    cards: string[],
    click(nameAction: number): void
}

function CardArtistsLayer(props: any) {
	const dispatch = useDispatch();
	const artists = useSelector((state: RootState) => state.artists.artists);
	useEffect(() => {
		getArtists().then((artistsList) => {
			if (artistsList) {
                dispatch(updateArtists(artistsList));
            }
		});
	}, []);

    const [genreIndex, setGenreIndex] = useState('');
    
    const genreHandle = (genreIndex: string) => {
        setGenreIndex(genreIndex);
    }

    const onClick = (artistId : number) =>{
        console.log('artists: ', artistId);
        props.click(artistId)
    };

    const containerArtist = {
        borderRadius: '2%',
        marginTop: '20px',
    };

    const containerLinkHead = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    };

    const typographyContent = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        margin: '20px'
    };

    const getArtistByGenre = (genre: string) => {
        let listArtist = [];
        if (genre){
            listArtist = artists.filter(artist => ((artist.genres|| []).includes(genre)))
        } else {
            listArtist = artists
        }
        return listArtist;
    }
    const artistElements = (genre: string) => {
        let listArtist = getArtistByGenre(genre);
        let result = null;
        result = (
            (listArtist.length == 0) ? <>
                <Typography variant="h6" style={typographyContent}>
                There are no Artists in this section
                </Typography>
            </> : 
            listArtist.map((artist: any) => (
                <Grid item xs={1.5} sm={1.5} md={1.5} key={artist.id} >
                <CardsArtist data={artist} key={artist.id} onClick={() => { onClick(artist.id) }}></CardsArtist>
                </Grid>
            ))
        )
        return result;
    }
  return (
    <Grid container style={containerArtist}>
            <Grid item xs={10} sm={10} md={8} >
                <h1>Top Artist</h1>
            </Grid>
            <Grid item xs={2} sm={2} md={4} style={containerLinkHead}>
                <GenreMenu click={genreHandle} ></GenreMenu>
            </Grid>
            <Grid item xs={12}>
                <Grid container spacing={{ xs: 1, md: 1 }} columns={{ xs: 1, sm: 1.5, md: 1.5 }}>
                    <Grid item xs={1.5} sm={1.5} md={1.5} >
                        <CardsArtist data={{'id': '-1','name': 'All Artist', 'image':'/icoArtist.png'}} onClick={() => { onClick(-1) }}></CardsArtist>
                    </Grid>
                    {artistElements(genreIndex)}
                </Grid>
            </Grid>
        </Grid>
  )
}

export default CardArtistsLayer