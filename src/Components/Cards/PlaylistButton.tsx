import React, { useState } from 'react';
import AddBoxIcon from '@mui/icons-material/AddBox';
import IconButton from '@mui/material/IconButton';
import IndeterminateCheckBoxIcon from '@mui/icons-material/IndeterminateCheckBox';
import { Stack, Alert } from '@mui/material';

export interface PlaylistButtonProps {
  song: PlayerSong;
  handleSelected(song: PlayerSong, isAdding: boolean): void;
}

export default function PlaylistButton({ song, handleSelected }: PlaylistButtonProps) {
  const [stateMessage, setStateMessage] = useState(false);
  const [buttonChecked, setButtonChecked] = useState(song.inPlaylist);

  const save = () => {
    handleSelected(song, buttonChecked);
	showSuccessMessage();
	setButtonChecked(!buttonChecked);
  };

  const showSuccessMessage = () => {
    setStateMessage(true);
    setTimeout(() => {
      setStateMessage(false);
    }, 2000);
  };
  return (
    <>
      {stateMessage ?
        <Stack sx={{ width: '100%' }} spacing={2}>
          <Alert severity="success">{buttonChecked ? 'to playlist' : 'out of playlist'}</Alert>
        </Stack>:<></>
      }
      <IconButton aria-label="add-playlist" onClick={save}>
		{buttonChecked ? <IndeterminateCheckBoxIcon /> : <AddBoxIcon />}
      </IconButton>
    </>
  );
}
