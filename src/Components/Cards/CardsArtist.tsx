import {  CardContent, CardMedia, Typography , Box, styled, } from "@mui/material";

import * as React from "react";
import { useEffect, useState } from "react";
import { getFile } from "../../Services/StorageService";

const Card = styled('div')(
	({ theme }) => `
	background-color: ${theme.palette.divider};
    border-radius: 10px;
    animation-duration: 0.5s;
    animation-name: animate-fade;
    animation-delay: 0.5s;
    animation-fill-mode: backwards;

    &:hover {
        background-color: ${theme.palette.secondary.main};
        padding-right: 24px;
        padding-left:10px;
        color: white;
    }
      
    @keyframes animate-fade {
        0% {
          opacity: 0;
        }
        100% {
          opacity: 1;
        }
    }
  `,
);

const CardsArtist = (props: any) => {

    const [img, setImg] = useState("");

	useEffect(() => {
        setImg(props.data.image);
        getFile(props.data.image).then((img_src) => {
            if (img_src) {
                setImg(img_src);
            }
        });
	}, []);

    const cardContentStyle = {
        display: 'flex',
        transitionDuration: '0.3s',
        alignItems: 'center'
    };

    const cardMedia = {
        marginLeft: '10px',
        borderRadius: '20%',
        boxShadow: '0px 5px 1px -1px rgba(0,0,0,0.2),0px 3px 8px 0px rgba(0,0,0,0.14),0px 7px 30px 0px rgba(0,0,0,0.12)'
    };
    
    return(
        <Card style={cardContentStyle} onClick={() => { props.onClick(props.data.id) }}>
            <CardMedia 
                style={cardMedia}
                component="img"
                sx={{ width: '50px', height: '50px' }}
                image={img}
                alt="Artist"
            />
            <CardContent sx={{ flex: '1 0 auto' }}>
                <Typography component="div" variant="subtitle1">
                    {props.data.name}
                </Typography>
            </CardContent>
        </Card>
    );
}
export default CardsArtist;