import { Box, styled, CardContent, CardMedia, Grid, IconButton, Typography, useTheme } from "@mui/material";
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import IndeterminateCheckBoxIcon from '@mui/icons-material/IndeterminateCheckBox';

import * as React from "react";
import { useDispatch } from 'react-redux';
import { useState, useEffect } from "react";

const Card = styled('div')(
	({ theme }) => `
	background-color: ${theme.palette.divider};
    border-radius: 10px;
    margin: 10px;
    margin-right: 80px;
    padding: 10px;
    transition-duration: 0.3s;
    animation-duration: 0.5s;
    animation-name: animate-fade;
    animation-delay: 0.5s;
    animation-fill-mode: backwards;

    &:hover {
        background-color: ${theme.palette.action.hover};
        padding-right: 15px;
        padding-left: 5px;
    }

    @keyframes animate-fade {
        0% {
          opacity: 0;
        }
        100% {
          opacity: 1;
        }
    }
  `,
);

export interface CardsSongsProps {
    song: any;
    handleRemove(song: PlayerSong): void;
  }


const CardsSongs = ({ song, handleRemove }: CardsSongsProps) => {
    
    //const [playerSongList, setPlayerSongList] = useState("");
    const CardMediaStyle = {
        width: '100px', 
        height: '100px', 
        borderRadius: '10px',                   
        boxShadow: '0px 5px 1px -1px rgba(0,0,0,0.2),0px 3px 8px 0px rgba(0,0,0,0.14),0px 7px 30px 0px rgba(0,0,0,0.12)'
    }

    const changeButton = () => {
        handleRemove(song);
    };

    return(
        <Grid container bgcolor={'transparent'} >
            <Grid item xs={12} >
                <Card>
                    <Grid container >
                        <Grid item xs={2} sm={1} md={1}>
                            <CardContent>1</CardContent>
                        </Grid>
                        <Grid item xs={10} sm={3} md={1.5}>
                        <CardMedia
                                component="img"
                                style={CardMediaStyle}
                                image= {song.image}
                                alt="Image Album" />
                        </Grid>     
                        <Grid item xs={12} sm={5} md={3.5}>
                            <CardContent sx={{ flex: '1 0 auto' , paddingTop:'4%'}}>
                                <Typography component="div" variant="subtitle1" fontWeight={700}>
                                    {song.title}
                                </Typography>
                                <Typography variant="caption" component="div">
                                    {song.autor}
                                </Typography>
                            </CardContent>
                        </Grid> 
                        <Grid item xs={12} sm={5} md={3}>
                            <CardContent sx={{ flex: '1 0 auto' , paddingTop:'4%'}}>
                                <Typography component="div" variant="subtitle1" fontWeight={700}>
                                    {song.genre}
                                </Typography>
                            </CardContent>
                        </Grid>       
                        <Grid item xs={12} sm={3} md={3}>
                            <Box sx={{ display: 'flex', alignItems: 'center', pl: 1, pb: 1 , justifyContent: 'center', height: '100%'}}>
                                <Typography variant="subtitle1" component="div" sx={{ marginRight: '1vh' }}>
                                    3:41
                                </Typography>
                                <IconButton aria-label="play/pause">
                                    <PlayArrowIcon sx={{ height: 38, width: 38 }} />
                                </IconButton>
                                <IconButton aria-label="play/pause" onClick={changeButton}>
                                    <IndeterminateCheckBoxIcon sx={{ height: 30, width: 30 }} />
                                </IconButton>
                            </Box>
                        </Grid> 
                    </Grid>
                </Card>
            </Grid>
        </Grid>       
    );
}

export default CardsSongs;