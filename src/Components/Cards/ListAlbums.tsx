import React, { useEffect, useState } from 'react';
import CardAlbum from './CardAlbum';
import { Grid, Typography } from '@mui/material';
import { useDispatch } from 'react-redux';
import { getArtistAlbums, getAlbums } from '../../Services/StorageService';
import { updateList } from '../../Services/Reducers/reproductionSlice';

function ListAlbums(props:any) {
  const dispatch = useDispatch();
  const [albums, setAlbums] = useState<Album[]>([]);
  let artistPlayList: PlayerSong[] = [];

  useEffect(() => {
    if (props.artistId === -1){
      updateAlbumsAll();
    } else {
      updateAlbums();
    }
  }, [props.artistId]);

  const updateAlbums = () => {
    getArtistAlbums(props.artistId).then((albums) => {
      if (albums) {
        setAlbums(albums);
      }
    });
  };

  const updateAlbumsAll = () => {
        getAlbums().then((albums: any) => {
          setAlbums(albums);
        })
        
  };
  const handlePlayerSong = (list: PlayerSong[], albumId: number) => {
    artistPlayList = [...artistPlayList, ...list];
    if (albumId === albums[albums.length - 1].id) {
      dispatch(updateList(artistPlayList));
    }
  }

  return (
    <Grid>
      {albums.length == 0 ?
        <>
          <Typography variant="h6">There are no Albums in this section</Typography>
        </> :
        albums.map((album) =>
          <CardAlbum
            key={album.id}
            album={album}
            handlePlayerSongList={handlePlayerSong}
            currentTrack={props.currentTrack}
            pausedTrack={props.pausedTrack}
            audioTrack={props.audioTrack}
            setPausedTrack={props.setPausedTrack}
          />
        )}
    </Grid>
  );
}

export default ListAlbums;
