import { Box, Container, Typography } from "@mui/material";
import * as React from "react";

const CardsGenre = () => {
    const boxGenre = {
        borderRadius: '10%',
        marginBottom: '13px',
        marginRight: '-30px', 
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    };

    return(
        <Box gridTemplateColumns="repeat(12, 1fr)"  display={{ xs: "block", sm: "block", md: "grid" }}>
            <Box gridColumn="span 8">
                <Container maxWidth="sm">
                    <Box sx={{ bgcolor: '#8d6e63', height: '10vh' }} style={boxGenre}>
                        <Typography gutterBottom variant="h6" fontWeight={700} color='white'>Pop</Typography>
                    </Box>
                </Container>
            </Box>
            <Box gridColumn="span 4">
                <Container maxWidth="sm">
                    <Box sx={{ bgcolor: '#607d8b', height: '10vh' }} style={boxGenre}> 
                    <Typography gutterBottom variant="h6" fontWeight={700} color='white'>Rock</Typography>
                    </Box>
                </Container>
            </Box>
            <Box gridColumn="span 4">
                <Container maxWidth="sm">
                    <Box sx={{ bgcolor: '#827717', height: '10vh' }} style={boxGenre}>
                    <Typography gutterBottom variant="h6" fontWeight={700} color='white'>Latin</Typography>
                    </Box>
                </Container>
            </Box>
            <Box gridColumn="span 8">
                <Container maxWidth="sm">
                    <Box sx={{ bgcolor: '#e65100', height: '10vh' }} style={boxGenre}>
                    <Typography gutterBottom variant="h6" fontWeight={700} color='white'>Acoustic</Typography>
                    </Box>
                </Container>
            </Box>
        </Box>
    );
}

export default CardsGenre;