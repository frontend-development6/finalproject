import localforage from 'localforage';

const ARTIST = 'artist';
const SONG = 'song';
const ALBUM = 'album';
const PLAYLIST = 'playlist';

export function initStorage() {
  localforage.setDriver([localforage.INDEXEDDB, localforage.LOCALSTORAGE, localforage.WEBSQL]);
}

export function saveImage(image: string | ArrayBuffer | null, key: string) {
  localforage.setItem(key, image);
}

export function saveMp3File(file: string | ArrayBuffer | null, key: string) {
  localforage.setItem(key, file);
}

export async function saveArtist(artist: Artist) {
  return new Promise<Artist[]>((resolve) => {
    localforage.getItem<Artist[]>(ARTIST).then((artists) => {
      if (artists) {
        const id = getArtistId(artists);
        artist.id = id;
        artists.push(artist);
      } else {
        artist.id = 1;
        artists = [artist];
      }

      localforage.setItem(ARTIST, artists).then((artists) => {
        resolve(artists);
      });
    });
  });
}

function getArtistId(artists: Artist[]) {
  return artists[artists.length - 1].id + 1;
}

export async function saveSong(song: Song) {
  return new Promise<Song[]>((resolve) => {
    localforage.getItem<Song[]>(SONG).then((songs) => {
      if (songs) {
        const id = getSongId(songs);
        song.id = id;
        songs.push(song);
      } else {
        song.id = 1;
        songs = [song];
      }

      localforage.setItem(SONG, songs).then((songs) => {
        resolve(songs);
      });
    });
  });
}

function getSongId(songs: Song[]) {
  return songs[songs.length - 1].id + 1;
}

export function setSongPlaylistState(songId: number, newState: boolean) {
  localforage.getItem<Song[]>(SONG).then((songs) => {
    if (songs) {
      for (let i = 0; i < songs.length; i++) {
        if (songs[i].id === songId) {
          songs[i].inPlayList = newState;
        }
      }
    }

    localforage.setItem(SONG, songs).then((songs) => {
      console.log(songs);
    });
  });
}

export async function saveAlbum(album: Album) {
  return new Promise<Album[]>((resolve) => {
    localforage.getItem<Album[]>(ALBUM).then((albums) => {
      if (albums) {
        const id = getIdAlbum(albums);
        album.id = id;
        albums.push(album);
      } else {
        album.id = 1;
        albums = [album];
      }

      localforage.setItem(ALBUM, albums).then((albums) => {
        resolve(albums);
      });
    });
  });
}

function getIdAlbum(albums: Album[]) {
  return albums[albums.length - 1].id + 1;
}

export function getSong() {
  return localforage.getItem<Song[]>(SONG);
}

export function getFile(key: string) {
  return localforage.getItem<string | null>(key);
}

export function getArtists() {
  return localforage.getItem<Artist[]>(ARTIST);
}

export function getArtistAlbums(artistId: number) {
  return new Promise<Album[]>((resolve) => {
    localforage.getItem<Album[]>(ALBUM).then((albums) => {
      if (albums) {
        const albumsFromArtist = albums.filter((album) => album.artist == artistId);
        resolve(albumsFromArtist);
      } else {
        resolve([]);
      }
    });
  });
}

export function getAlbums() {
  return localforage.getItem<Album[]>(ALBUM);
}

export async function getPlayerSongsByArtist(artistId: number) {
  const artists = await localforage.getItem<Artist[]>(ARTIST);
  const albums = await localforage.getItem<Album[]>(ALBUM);
  const songs = await localforage.getItem<Song[]>(SONG);

  const artist = artists?.find((artist) => artist.id == artistId);

  const result: PlayerSong[] = [];
  if (songs) {
    for (let i = 0; i < songs.length; i++) {
      if (songs[i].artist == artistId) {
        const album = albums?.find((album) => album.id == songs[i].album);
        const albumImage = album ? await localforage.getItem<string | null>(album.image) : '';
        const musicFile = await localforage.getItem<string | null>(songs[i].file);

        result.push({
          songId: songs[i].id,
          file: musicFile,
          image: albumImage,
          genre: songs[i].genre,
          title: songs[i].title,
          autor: artist ? artist.name : '',
          members: artist ? artist?.members : '',
          album: album ? album.title : '',
          year: album ? album.year : 0,
          inPlaylist: songs[i].inPlayList
        });
      }
    }
  }

  return result;
}

export async function getPlayerSongsByAlbum(albumId: number) {
  const artists = await localforage.getItem<Artist[]>(ARTIST);
  const albums = await localforage.getItem<Album[]>(ALBUM);
  const songs = await localforage.getItem<Song[]>(SONG);

  const album = albums?.find((album) => album.id == albumId);
  const artist = artists?.find((artist) => artist.id == album?.artist);

  const albumImage = album ? await localforage.getItem<string | null>(album.image) : '';

  const result: PlayerSong[] = [];
  if (songs) {
    for (let i = 0; i < songs?.length; i++) {
      if (songs[i].album == albumId) {
        const musicFile = await localforage.getItem<string | null>(songs[i].file);
        result.push({
          songId: songs[i].id,
          file: musicFile,
          image: albumImage,
          genre: songs[i].genre,
          title: songs[i].title,
          autor: artist ? artist.name : '',
          members: artist ? artist.members : '',
          album: album ? album.title : '',
          year: album ? album.year : 0,
          inPlaylist: songs[i].inPlayList
        });
      }
    }
  }

  return [...result];
}

export async function getPlayerSongBySongId(songId: number) {
  const artists = await localforage.getItem<Artist[]>(ARTIST);
  const albums = await localforage.getItem<Album[]>(ALBUM);
  const songs = await localforage.getItem<Song[]>(SONG);

  const song = songs?.find((song) => song.id == songId);

  if (song) {
    const album = albums?.find((album) => album.id == song.album);
    const artist = artists?.find((artist) => artist.id == song.artist);

    const albumImage = album ? await localforage.getItem<string | null>(album.image) : '';
    const musicFile = await localforage.getItem<string | null>(song.file);

    const result: PlayerSong = {
      songId: song.id,
      file: musicFile,
      image: albumImage,
      genre: song.genre,
      title: song.title,
      autor: artist ? artist.name : '',
      members: artist ? artist?.members : '',
      album: album ? album.title : '',
      year: album ? album.year : 0,
      inPlaylist: song.inPlayList
    };
    return [result];
  }

  return null;
}

export async function savePlaylist(playlist: Playlist) {
  return new Promise<Playlist[]>((resolve) => {
    localforage.getItem<Playlist[]>(SONG).then((playlists) => {
      if (playlists) {
        const id = getIdPlaylist(playlists);
        playlist.id = id;
        playlists.push(playlist);
      } else {
        playlist.id = 1;
        playlists = [playlist];
      }

      localforage.setItem(PLAYLIST, playlists).then((playlists) => {
        resolve(playlists);
      });
    });
  });
}

function getIdPlaylist(playlist: Playlist[]) {
  return playlist[playlist.length - 1].id + 1;
}
