import { configureStore } from '@reduxjs/toolkit';
import themeReducer from './Reducers/themeSlice';
import artistsReducer from './Reducers/artistsSlice';
import albumsReducer from './Reducers/albumsSlice';
import songsReducer from './Reducers/songsSlice';
import reproductionListReducer from './Reducers/reproductionSlice';
import playListReducer from './Reducers/playListSlice';

export const store = configureStore({
  reducer: {
    theme: themeReducer,
    artists: artistsReducer,
    albums: albumsReducer,
    songs: songsReducer,
    reproduction: reproductionListReducer,
	playlist: playListReducer
  }
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
