import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface ArtistsState {
  artists: Artist[];
}

const initialState: ArtistsState = {
	artists:[] 
};

export const artistsSlice = createSlice({
  name: 'artists',
  initialState,
  reducers: {
    updateArtists: (state, action: PayloadAction<Artist[]>) => {
		state.artists = action.payload;
    },
  }
});

export const { updateArtists } = artistsSlice.actions;

export default artistsSlice.reducer;
