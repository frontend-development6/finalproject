import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface AlbumsState {
  albums: Album[];
}

const initialState: AlbumsState = {
	albums:[] 
};

export const albumsSlice = createSlice({
  name: 'albums',
  initialState,
  reducers: {
    updateAlbums: (state, action: PayloadAction<Album[]>) => {
		state.albums = action.payload;
    },
  }
});

export const { updateAlbums } = albumsSlice.actions;

export default albumsSlice.reducer;
