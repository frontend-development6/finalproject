import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface SongsState {
  songs: Song[];
}

const initialState: SongsState = {
	songs:[] 
};

export const songsSlice = createSlice({
  name: 'songs',
  initialState,
  reducers: {
    updateSongs: (state, action: PayloadAction<Song[]>) => {
		state.songs = action.payload;
    },
  }
});

export const { updateSongs } = songsSlice.actions;

export default songsSlice.reducer;