import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface ReproductionListState {
  list: PlayerSong[];
}

const initialState: ReproductionListState = {
  list: []
};

export const reproductionListSlice = createSlice({
  name: 'reproductionList',
  initialState,
  reducers: {
    updateList: (state, action: PayloadAction<PlayerSong[]>) => {
      state.list = action.payload;
    },
  }
});

// Action creators are generated for each case reducer function
export const { updateList } = reproductionListSlice.actions;

export default reproductionListSlice.reducer;
