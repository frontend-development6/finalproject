import { createSlice } from '@reduxjs/toolkit';
import { PaletteMode } from '@mui/material';

export interface ThemeState {
  mode: PaletteMode;
}

const initialState: ThemeState = {
  mode: 'dark'
};

export const themeSlice = createSlice({
  name: 'theme',
  initialState,
  reducers: {
    darkMode: (state) => {
      state.mode = 'dark';
    },
    lightMode: (state) => {
      state.mode = 'light';
    }
  }
});

// Action creators are generated for each case reducer function
export const { darkMode, lightMode } = themeSlice.actions;

export default themeSlice.reducer;
