import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface MyPlayListState {
  list: PlayerSong[];
}

const initialState: MyPlayListState = {
  list: []
};

export const myPlayListSlice = createSlice({
  name: 'myPlayListList',
  initialState,
  reducers: {
	addElement: (state, action: PayloadAction<PlayerSong>) => {
		state.list.push(action.payload);
	},
	deleteElement: (state, action: PayloadAction<PlayerSong>) => {

		let index = state.list.findIndex((song) => song.title == action.payload.title);
		state.list.splice(index, 1);
	}
  }
});

// Action creators are generated for each case reducer function
export const { addElement, deleteElement } = myPlayListSlice.actions;

export default myPlayListSlice.reducer;
