type Song = {
	id: number,
	title: string,
	genre: string,
	releaseYear: number,
	artist: number,
	album: number,
	duration: number,
	file: string,
	inPlayList: boolean,
}