type PlayerSong = {
	songId: number,
	file: string | null,
	image: string | null,
	genre: string,
	title: string,
	autor: string,
	members: string,
	album: string,
	year: number,
	inPlaylist: boolean
}