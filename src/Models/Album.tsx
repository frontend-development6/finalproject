type Album = {
	id: number;
	title: string;
    year: number;
	genre: string;
	image: string;
	artist: number;
}