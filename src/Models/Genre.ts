export const genres = [
	'Pop',
	'Rock',
	'Electronic',
	'Classical',
	'Jazz',
	'Soul',
	'Metal',
	'K-Pop',
	'Gospel',
	'Funk',
	'Techno'
]