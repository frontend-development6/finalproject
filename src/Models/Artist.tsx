type Artist = {
	id: number;
	name: string;
	genres: string[];
	members: string;
	webSite: string;
	image: string;
}